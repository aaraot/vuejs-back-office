# Vue.js Back Office

Back Office build in Laravel and Vue.js.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

### Laravel

This project requires Laravel.

* To check that you have the laravel installed, run laravel -V in a terminal/console window.
* To install laravel run:

```
composer global require laravel/installer
```

### Composer

Laravel utilizes Composer to manage its dependencies. So, before using Laravel, make sure you have Composer installed on your machine.

* To check that you have the composer installed, run composer -V in a terminal/console window.
* To get Composer, go to [getcomposer.org](https://getcomposer.org/).

#### Node.js

This project requires Node.js.

* To check that you have the Node.js installed, run node -v in a terminal/console window.
* To get Node.js, go to [nodejs.org](https://nodejs.org/en/).

#### npm package manager

This project depend on features and functionality provided by libraries that are available as [npm packages](https://docs.npmjs.com/about-npm/index.html). To download and install npm packages, you must have an npm package manager.

This Quick Start uses the [npm client](https://docs.npmjs.com/cli/install) command line interface, which is installed with Node.js by default.

To check that you have the npm client installed, run npm -v in a terminal/console window.

### Installing

In order to compile scripts and styles, you can run the following commands in a terminal/console window:

```
// This will install all the dev dependencies under composer.json file

composer install

// This will install all the dev dependencies under package.json file

npm install

// Create a DB before running the following commands
php artisan migrate

php artisan db:seed --class=UsersTableSeeder

// This will run and watch all the gulp tasks

npm run watch
```

## Deployment

If you want to deploy this code into production run:

```
npm run prod
```

## Built With

* [Node.js](https://nodejs.org/en/) - is an open-source, cross-platform JavaScript run-time environment that executes JavaScript code outside of a browser.
* [npm](https://www.npmjs.com/) - is a package manager for the JavaScript programming language. 

## Authors

* **Aarão Teixeira** - [aaraot](https://gitlab.com/aaraot)

## Inspiration

* **CSS Animation** - [Clocks](https://cssanimation.rocks/clocks/)
* **Tyler West** - [Clock](https://github.com/SlyTy7/clock)
* **Martin Angelov** - [Digital Clock](https://tutorialzine.com/2013/06/digital-clock)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
