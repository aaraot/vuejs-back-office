<?php

namespace App\Http\Controllers;

use App\Notifications\Register;
use App\User;
use App\UserAPI;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user->email = $request->get('email');
        $user->notify(new Register());

        return response()->json(['message' => 'An email has been sent!'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $rules = [];

        if ($request->get('emailChanged')) {
            $rules = [
                'name' => 'required|max:191',
                'email' => 'required|unique:users|email'
            ];
        } else {
            $rules = ['name' => 'required|max:191'];
        }

        Validator::make($request->all(), $rules)->validate();

        DB::beginTransaction();

        try {
            $user = User::find($id);

            $user->name = $request->get('name');
            $user->email = $request->get('email');

            $user->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['user' => $user, 'message' => 'User has been updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();

        try {
            $user = User::find($id);
            $user->delete();

            $userApi = UserAPI::find($user->id);

            if ($userApi)
                $userApi->delete();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['message' => 'User has been deleted!'], 200);
    }
}
