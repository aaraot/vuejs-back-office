<?php

namespace App\Http\Controllers;

use App\Area;
use App\Client;
use App\Contact;
use App\Route;
use Auth;
use Illuminate\Http\Response;

class ApiService extends Controller
{
    /**
     * Set the user ready by configuring the password
     *
     * @param integer $id
     * @return Response
     */
    public function getReady($id)
    {
        return view('get-ready', ['id' => $id]);
    }
}
