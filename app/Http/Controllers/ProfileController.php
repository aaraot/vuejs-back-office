<?php

namespace App\Http\Controllers;

use App\User;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if (!$request->has('password') && $request->get('password') !== '') {
            Validator::make($request->all(), [
                'name' => 'required|min:6|max:191'
            ])->validate();
        } else {
            Validator::make($request->all(), [
                'password' => 'required|min:6',
                'confirmPassword' => 'required|min:6|same:password'
            ])->validate();
        }

        DB::beginTransaction();

        try {
            $user = User::find(base64_decode($id));

            if (!$request->has('password')) {
                $user->name = $request->get('name');
            } else {
                $user->password = bcrypt($request->get('password'));
            }

            $user->save();

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();

            return response()->json(['message' => $e->getMessage(), 422]);
        }

        return response()->json(['message' => 'Your profile has been updated!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
