<?php

namespace App\Http\Controllers;

use App\Area;
use App\AreaContent;
use App\Client;
use App\Contact;
use App\Equipment;
use App\EquipmentCategory;
use App\EquipmentDetailCover;
use App\EquipmentDetailSize;
use App\EquipmentDetailTech;
use App\Page;
use App\Quote;
use App\Route;
use App\SearchTip;
use App\Seo;
use App\Service;
use App\ServiceHighlight;
use App\SiteConfig;
use App\TopSearch;
use App\User;
use Auth;
use Illuminate\Http\Response;

class BOService extends Controller
{
    /**
     * Get authenticated user
     *
     * @param
     * @return Response
     */
    public function getUser()
    {
        Auth::user()->base64ID = base64_encode(Auth::user()->id);

        return Auth::user();
    }

    /**
     * Get all users
     *
     * @param
     * @return Response
     */
    public function getUsers()
    {
        return User::all();
    }
}
