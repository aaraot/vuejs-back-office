<?php

/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/

Auth::routes(['verify' => true]);

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'verified'], function () {
    Route::get('/', 'HomeController@index')->middleware('auth');

    // RESOURCES
    Route::resource('profile', 'ProfileController');
    Route::resource('users', 'UsersController');

    // GETS
    Route::get('bo/get-user', 'BOService@getUser');
    Route::get('bo/get-users', 'BOService@getUsers');
});

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/
