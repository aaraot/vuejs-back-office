<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'API') }}</title>

    <!-- Font -->
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,300i,400,400i,500,500i" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">

    <!-- Theme -->
    <link href="{{ asset('/css/auth-theme.css') }}" rel="stylesheet">

    <!-- Page CSS -->
@yield('css')

<!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    </script>
</head>
<body>
<div id="app" class="login"
     style="background-image: linear-gradient(134.86deg, #0575E6 0%, #499DF1 100%), url(https://unsplash.it/g/1920/1080/?random);background-blend-mode: color;">
    @yield('content')
</div>

<script src="{{ asset('/frameworks/jquery-1.12.4/jquery.min.js') }}"></script>

<!-- Scripts -->
<script src="{{ asset('js/auth.js') }}"></script>

<!-- Page Scripts -->
@yield('js')
</body>
</html>
