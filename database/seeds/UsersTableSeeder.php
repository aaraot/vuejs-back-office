<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Aarão Emanuel Santos Teixeira',
            'email' => 'aarao.teixeira@gmail.com',
            'email_verified_at' => '2019-07-12 09:29:39',
            'password' => '$2y$10$5QwF.eZTDIV1dtwv2ridXu.tMEOWBJ9lUoCGzYwEcPbzfn91PJF26',
            'remember_token' => 'ST3DHCL7FT2gyoJEIqxIp034kyVX0oeFRlENNbxW53jHEolhVBZb7wE0dG5d',
            'created_at' => '2019-07-12 09:29:33',
            'updated_at' => '2019-07-12 09:29:39'
        ]);
    }
}
